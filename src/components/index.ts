// 用于注册全局公共组件
import SvgIcon from './SvgIcon/index.vue'
import type { App, Component } from 'vue'
const components: { [name: string]: Component } = { SvgIcon }
export default {
  install(app: App) {
    Object.keys(components).forEach((key: string) => {
      console.log('key, components[key]', key, components[key])

      app.component(key, components[key])
    })
  },
}
