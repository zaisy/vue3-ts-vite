import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import 'virtual:svg-icons-register'
import gloablComponent from './components/index'

const app = createApp(App)

app.use(gloablComponent)
app.mount('#app')
