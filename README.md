## VUE3+TS+VITE

```sh
  npm i -g pnpm
  pnpm create vite
```

## 1、eslint配置

```sh
  pnpm i eslint -D
  npx eslint --init
  pnpm install -D eslint-plugin-import eslint-plugin-vue eslint-plugin-node eslint-plugin-prettier eslint-config-prettier eslint-plugin-node @babel/eslint-parser
```

- 修改.eslintrc.cjs 配置文件

## 2、prettier配置

```sh
  pnpm install -D eslint-plugin-prettier prettier eslint-config-prettier
```

## 3、stylelint配置

```sh
pnpm add stylelint postcss postcss-less postcss-html stylelint-config-prettier stylelint-config-recommended-less stylelint-config-standard stylelint-config-standard-vue stylelint-less stylelint-order -D
```

- stylelint-config-prettier 组件已经弃用，stylelint V15自带了相关功能

## 4、husky配置 强制让开发人员按照代码规范来提交

- pnpm install -D husky
- npx husky-init
- 在.husky/pre-commit文件添加如下命令

```sh
    #!/usr/bin/env sh
    . "$(dirname -- "$0")/_/husky.sh"
    pnpm run format
```

## 5、commitlint配置约束commit提交信息

```sh
  pnpm add @commitlint/config-conventional @commitlint/cli -D
  npx husky add .husky/commit-msg
```

- 在生成的 commit-msg 文件中添加下面的命令

```sh
    #!/usr/bin/env sh
    . "$(dirname -- "$0")/_/husky.sh"
    pnpm commitlint
```

## 6、强制使用 pnpm 配置 ，统一项目依赖版本

- 在根目录创建scritps/preinstall.js文件，添加下面的内容

```sh
if (!/pnpm/.test(process.env.npm_execpath || '')) {
  console.warn(
    `\u001b[33mThis repository must using pnpm as the package manager ` +
    ` for scripts to work properly.\u001b[39m\n`,
  )
  process.exit(1)
}
```

- 配置命令

```sh
"scripts": {
	"preinstall": "node ./scripts/preinstall.js"
}
```

## 7、SVG 图标配置

- pnpm install vite-plugin-svg-icons -D

## 8、vite构建时静态资源引入方式

```js
import vueLogo from '@/assets/icons/vue.svg'
<img :src="vueLogo" class="logo vue" alt="Vue logo" />
```
